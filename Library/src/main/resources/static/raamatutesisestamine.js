/*var refreshRaamatuandmed = async function() {
 	console.log("refreshRaamatuandmed läks käima")
 	var APIurl = "http://localhost:8080/library/raamatud" + "?otsing=" + otsitudRaamat
 	var request = await fetch(APIurl);
 	var raamatuandmed = await request.json();
 	console.log(raamatuandmed);

 	document.querySelector('#andmed3').innerHTML = ""

 	while (raamatuandmed.length > 0) {
 	    var otsing = raamatuandmed.shift()
 		document.querySelector('#andmed3').innerHTML += `<p>Lisatud raamatu pealkiri on "${otsing.title}" ja autor on ${otsing.author}</p>`
 	}
 }


setInterval(refreshRaamatuandmed, 1000)
*/

document.querySelector("#form3").onsubmit = function(event){
    event.preventDefault()

	var title = document.querySelector('#title3').value
	var author = document.querySelector('#author').value

	var summary = document.querySelector('#summary').value


    document.querySelector('#title3').value = ""
	document.querySelector('#author').value = ""

	document.querySelector('#summary').value = ""

	console.log(title, author, summary)

    var APIurl = location.origin + "/library/raamatud/new-book"
	fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({title: title, author: author, summary: summary}),
		headers: {
	        'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})
}

