/*var refreshKliendiandmed = async function(){
    console.log("refreshKliendiandmed läks käima")
    var APIurl = "http://localhost:8080/library/kliendid"
    var request = await fetch(APIurl);
    var kliendiandmed = await request.json();
    console.log(kliendiandmed);

    document.querySelector("#kliendiandmed").innerHTML = ""

    while (kliendiandmed.length > 0) {
        var otsing = kliendiandmed.shift()
        document.querySelector("#kliendiandmed").innerHTML += `<p>${otsing.nimi} laenutas raamatu "${otsing.title}"</p>`
    }
 }


 setInterval(refreshKliendiandmed, 1000)
*/

 document.querySelector("#form").onsubmit = function(event) {
     console.log("klientide function läks käima")
     event.preventDefault()

 	var nimi = document.querySelector('#nimi').value
 	var title = document.querySelector('#title').value
 	var telefonInt = document.querySelector('#telefon').value
 	var telefon = telefonInt.toString(8)
 	var tahtaegInt = document.querySelector('#tahtaeg').value
 	var tahtaeg = tahtaegInt.toString(8)


     document.querySelector('#nimi').value = ""
     document.querySelector('#title').value = ""
     document.querySelector('#telefon').value = ""
     document.querySelector('#tahtaeg').value = ""


     console.log(nimi, title, telefon, tahtaeg)

     var APIurl = location.origin + "/library/kliendid/new-client"
 	fetch(APIurl, {
 		method: "POST",
 		body: JSON.stringify({nimi: nimi, title: title, telefon: telefon, tahtaeg: tahtaeg}),
 		headers: {
 	        'Accept': 'application/json',
 			'Content-Type': 'application/json'
 		}
 	})
 }
