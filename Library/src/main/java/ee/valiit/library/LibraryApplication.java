package ee.valiit.library;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;


@SpringBootApplication
public class LibraryApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(LibraryApplication.class, args);
	}
	@Autowired
	JdbcTemplate jdbcTemplate;



	@Override
	public void run(String... args) throws Exception {
		System.out.println("Configure database tables");
		//jdbcTemplate.execute("DROP TABLE IF EXISTS raamatuandmed");
		jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS raamatuandmed (title TEXT, author TEXT, summary TEXT)");
		//jdbcTemplate.execute("DROP TABLE IF EXISTS kliendiandmed");
		jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS kliendiandmed (nimi TEXT, title TEXT, telefon TEXT, tahtaeg TEXT)");
	}


}
