package ee.valiit.library;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
@CrossOrigin


public class APIController {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @GetMapping("/library/raamatud")
    ArrayList<Book> allBooks(@RequestParam (value= "otsing") String otsing) {
        System.out.println("otsing: " + otsing);
        try {
            String sqlKask = "SELECT * FROM raamatuandmed WHERE title='"+otsing+"'";
            ArrayList<Book> raamatuandmed = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {
                String title = resultSet.getString("title");
                String author = resultSet.getString("author");
                String summary = resultSet.getString("summary");

                return new Book(title, author, summary);
            });

            return raamatuandmed;
        } catch (DataAccessException err) {
            System.out.println("BOOKS TABLE WAS NOT READY");
            return new ArrayList();
        }
    }

    @GetMapping("/library/kliendid")
    ArrayList<Klient> allClients(@RequestParam("otsing") String otsing) {
        try {
            String sqlKask = "SELECT * FROM kliendiandmed WHERE title='"+otsing+"'";
            ArrayList<Klient> kliendiandmed = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {
                String nimi = resultSet.getString("nimi");
                String title = resultSet.getString("title");
                String telefon = resultSet.getString("telefon");
                String tahtaeg = resultSet.getString("tahtaeg");
                return new Klient(nimi, title, telefon, tahtaeg);
            });
            return kliendiandmed;
        } catch (DataAccessException err) {
            System.out.println("CLIENTS TABLE WAS NOT READY");
            return new ArrayList();
        }
    }





    @PostMapping("library/raamatud/new-book")
    void newBook(@RequestBody Book book) {   //@Pathvariable ????????????
        String sqlKask = "INSERT INTO raamatuandmed (title, author, summary) VALUES" + "('" + book.getTitle() + "', '" + book.getAuthor() + "', '" + book.getSummary() + "')";
        jdbcTemplate.execute(sqlKask);
    }

    @PostMapping("library/kliendid/new-client")
    void uusKlient(@RequestBody Klient klient) {
        String sqlKask = "INSERT INTO kliendiandmed (nimi, title, telefon, tahtaeg) VALUES" + "('" + klient.getNimi() + "', '" + klient.getTitle() + "', '" + klient.getTelefon() + "', '" + klient.getTahtaeg() + "')";
        jdbcTemplate.execute(sqlKask);
    }


}
