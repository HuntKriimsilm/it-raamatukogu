package ee.valiit.library;

public class Klient {
    private String nimi;
    private String title;
    private String telefon;
    private String tahtaeg;


    public Klient(){};

    public Klient(String nimi, String title, String telefon, String tahtaeg){
        this.nimi = nimi;
        this.title = title;
        this.telefon = telefon;
        this.tahtaeg = tahtaeg;

    }

    public String getNimi() {
        return nimi;
    }

    public String getTitle() { return title; }


    public String getTelefon() {
        return telefon;
    }

    public String getTahtaeg() {
        return tahtaeg;
    }
}
